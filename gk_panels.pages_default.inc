<?php
/**
 * @file
 * gk_panels.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function gk_panels_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_gk_default';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'GK: Default',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'site_default';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'ecc3d344-31a9-45be-96e5-5c66add61cad';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-881a2b9b-88f0-41bd-8c13-813ac2743d8f';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '881a2b9b-88f0-41bd-8c13-813ac2743d8f';
    $display->content['new-881a2b9b-88f0-41bd-8c13-813ac2743d8f'] = $pane;
    $display->panels['content'][0] = 'new-881a2b9b-88f0-41bd-8c13-813ac2743d8f';
    $pane = new stdClass();
    $pane->pid = 'new-5f9a88f8-9803-4a38-b200-965eb101c26d';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5f9a88f8-9803-4a38-b200-965eb101c26d';
    $display->content['new-5f9a88f8-9803-4a38-b200-965eb101c26d'] = $pane;
    $display->panels['content_header'][0] = 'new-5f9a88f8-9803-4a38-b200-965eb101c26d';
    $pane = new stdClass();
    $pane->pid = 'new-164c8ef9-0fe6-4765-8981-0420d924000c';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_panels-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '164c8ef9-0fe6-4765-8981-0420d924000c';
    $display->content['new-164c8ef9-0fe6-4765-8981-0420d924000c'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-164c8ef9-0fe6-4765-8981-0420d924000c';
    $pane = new stdClass();
    $pane->pid = 'new-fc50e2db-29a8-46b3-a341-833cab352d45';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'fc50e2db-29a8-46b3-a341-833cab352d45';
    $display->content['new-fc50e2db-29a8-46b3-a341-833cab352d45'] = $pane;
    $display->panels['footer_bottom'][1] = 'new-fc50e2db-29a8-46b3-a341-833cab352d45';
    $pane = new stdClass();
    $pane->pid = 'new-9ad30285-f238-42a1-b3fe-4067234186c0';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '9ad30285-f238-42a1-b3fe-4067234186c0';
    $display->content['new-9ad30285-f238-42a1-b3fe-4067234186c0'] = $pane;
    $display->panels['header'][0] = 'new-9ad30285-f238-42a1-b3fe-4067234186c0';
    $pane = new stdClass();
    $pane->pid = 'new-681be1b1-009a-47fa-816e-12bf70588879';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '681be1b1-009a-47fa-816e-12bf70588879';
    $display->content['new-681be1b1-009a-47fa-816e-12bf70588879'] = $pane;
    $display->panels['header_top'][0] = 'new-681be1b1-009a-47fa-816e-12bf70588879';
    $pane = new stdClass();
    $pane->pid = 'new-b8fe05ac-1fec-4c72-a23b-63d490b41908';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b8fe05ac-1fec-4c72-a23b-63d490b41908';
    $display->content['new-b8fe05ac-1fec-4c72-a23b-63d490b41908'] = $pane;
    $display->panels['navigation'][0] = 'new-b8fe05ac-1fec-4c72-a23b-63d490b41908';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-fc50e2db-29a8-46b3-a341-833cab352d45';
  $handler->conf['display'] = $display;
  $export['site_template_panel_gk_default'] = $handler;

  return $export;
}
