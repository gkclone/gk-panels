<?php

/**
 * Preprocess variables for theme pane_content_header.
 */
function template_preprocess_pane_content_header(&$variables) {
  $variables['messages'] = theme('status_messages');
  $variables['title'] = drupal_get_title();
  $variables['title_attributes_array']['class'][] = 'PageTitle';
  $variables['tabs'] = menu_local_tabs();
  $variables['action_links'] = menu_local_actions();
  $variables['help'] = theme('help');

  // When page title is set to hide, make it invisible.
  // More accessible for screen readers and SEO.
  if (($node = menu_get_object()) && !empty($node->field_hide_title[LANGUAGE_NONE][0]['value'])) {
    $variables['title_attributes_array']['class'][] = 'is-hiddenVisually';
  }
}
