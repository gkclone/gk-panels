<?php

/**
 * @file
 * Plugin to handle the 'page' content type which allows the standard page
 * template variables to be embedded into a panel.
 */

$plugin = array(
  'title' => t('Page content header'),
  'single' => TRUE,
  'icon' => 'icon_page.png',
  'description' => t('A general page content header block that includes title, actions, tabs, messages and help.'),
  'category' => t('Page elements'),
  'render last' => TRUE,
);

/**
 * Output function for the 'pane_content_header' content type.
 *
 * Outputs the content header of the current page.
 */
function gk_panels_pane_content_header_content_type_render($subtype, $conf, $panel_messagesrgs) {
  $block = new stdClass();
  $block->content = theme('pane_content_header');
  return $block;
}

function gk_panels_pane_content_header_content_type_admin_info($subtype, $conf) {
  $block = new stdClass;
  $block->title = t('Page content header');
  $block->content = t('A general page content header block that includes title, actions, tabs, messages and help.');
  return $block;
}
